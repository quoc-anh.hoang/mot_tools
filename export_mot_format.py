import os 
import cv2
import numpy as np
import shutil
from tools.utils.config_utils import get_tools_elements
from tools.utils.infer_utils import infer_one_image
from tools.utils.visual import visual_one_image_detect, visual_one_image_tracking

#video_list_path la thu muc chua cac video can gan nhan
def main():
    body_model_name = 'yolov5-person'
    body_config = 'configs/yolov5-person.yaml'
    face_model_name = 'yolov5-face'
    face_config = 'configs/yolov5-face.yaml'
    tracking_type = 'bytetrack'
    track_config = 'configs/bytetrack.yaml'
    video_list_path = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking/comit_short_videos'

    body_detector = get_tools_elements('detect', body_model_name, body_config)
    face_detector = get_tools_elements('detect', face_model_name, face_config)

    videos_list = os.listdir(video_list_path)

    for video in videos_list:
        save_txt_path = os.path.join('output_tracking_txt', video + '.txt')
        if os.path.exists(save_txt_path):
            os.remove(save_txt_path)
        f = open(save_txt_path, 'w')
        print(save_txt_path)
        frame_id = 0
        vid = cv2.VideoCapture(os.path.join(video_list_path, video))
        tracker = get_tools_elements('tracking', tracking_type, track_config)
        while(True):
            ret, frame = vid.read()
            if not ret:
                break
            frame_id += 1
            out_body_detect = body_detector([frame])
            out_face_detect = face_detector([frame])
            
            if out_body_detect[0][0].shape[0] != 0 and out_face_detect[0][0].shape[0] != 0:
                all_boxes = np.concatenate((out_body_detect[0][0], out_face_detect[0][0]), axis=0)
                all_scores = np.concatenate((out_body_detect[1][0], out_face_detect[1][0]), axis=0)
                face_class = np.ones_like(out_face_detect[2][0])
                body_class = np.ones_like(out_body_detect[2][0]) * 2
                all_class = np.concatenate((body_class, face_class), axis=0)
            
            elif out_body_detect[0][0].shape[0] != 0 and out_face_detect[0][0].shape[0] == 0:
                all_boxes = out_body_detect[0][0]
                all_scores = out_body_detect[1][0]
                all_class = np.ones_like(out_body_detect[2][0]) * 2

            elif out_body_detect[0][0].shape[0] == 0 and out_face_detect[0][0].shape[0] != 0:
                all_boxes = out_face_detect[0][0]
                all_scores = out_face_detect[1][0]
                all_class = np.ones_like(out_face_detect[2][0])

            outputs = tracker.update(all_boxes, all_scores, all_class)
            if len(outputs) > 0:
                track_boxes = outputs[:, :4]
                track_ids = outputs[:, 4]
                track_confs = outputs[:, 5]
                track_cls = outputs[:, 6]
                
                for i in range(len(track_boxes)):
                    txt_save = '{},{},{},{},{},{},1,{},1.0\n'.format(
                    frame_id, int(track_ids[i]), 
                    track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1], int(track_cls[i])
                    )
                    f.write(txt_save)
        f.close()

def copy_to_folder_ann():
    annotations_folder = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking/pre_predict_cvat_mot'
    save_folder = '/home/hqanh/Documents/cvat_annotations/face_and_body_tracking'
    ann_list = os.listdir(annotations_folder)
    for ann in ann_list:
        vid_name = ann[:-4]
        save_ann_path = os.path.join(save_folder, vid_name)
        os.makedirs(os.path.join(save_ann_path, 'gt'), exist_ok=True)
        f = open(os.path.join(save_ann_path, 'gt', 'labels.txt'), 'w')
        f.write('face\n')
        f.write('body')
        f.close()
        shutil.copy(os.path.join(annotations_folder, ann), os.path.join(save_ann_path, 'gt', 'gt.txt'))

if __name__ == '__main__':
    # main()
    copy_to_folder_ann()