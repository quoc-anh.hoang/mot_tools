python save_crop_detects.py \
--data-path '/home/hqanh/Videos/vlc-record-2024-01-11-16h47m55s-LND-V6030R 20240111 154415.avi-.mp4' \
--detect-model yolov5-face \
--detect-config configs/yolov5-face.yaml \
--tracking-type  bytetrack \
--tracking-config configs/bytetrack.yaml \
--visual \
--skip-frame 0 