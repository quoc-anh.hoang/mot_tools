import os 

class BaseDataset(object):
    def __init__(self, data_dir):
        self.data_dir = data_dir
        if not os.path.exists(self.data_dir):
            raise IOError('dataset dir: {} is non-exist'.format(
                            self.data_dir))
        self.n_vids = 0

        #{vid/image_folder path : annotation path}
        self.vid_dict = {}

    def load_dataset_info(self):
        raise NotImplementedError("load_dataset_info function need to be override!")

class VisDroneData(BaseDataset):
    def __init__(self, data_dir, phase='all'):
        super(VisDroneData, self).__init__(data_dir)
        self.phase = phase 
        self.train_path = os.path.join(data_dir, 'MOT/VisDrone2019-MOT-train/')
        self.val_path = os.path.join(data_dir, 'MOT/VisDrone2019-MOT-val')
        if not os.path.exists(self.train_path) or not os.path.exists(self.val_path):
            raise IOError('dataset dir: {} or {} is non-exist'.format(
                            self.train_path, self.val_path))
        self.load_dataset_info()

    def load_dataset_info(self):
        for train_vid in os.listdir(os.path.join(self.train_path, 'sequences')):
            ann_name = train_vid + '.txt'
            ann_path = os.path.join(self.train_path, 'annotations', ann_name)
            if os.path.exists(ann_path) and (self.phase == 'train' or self.phase == 'all'):
                self.vid_dict[os.path.join(self.train_path, 'sequences', train_vid)] = ann_path

        for val_vid in os.listdir(os.path.join(self.val_path, 'sequences')):
            ann_name = val_vid + '.txt'
            ann_path = os.path.join(self.val_path, 'annotations', ann_name)
            if os.path.exists(ann_path) and (self.phase == 'val' or self.phase == 'all'):
                self.vid_dict[os.path.join(self.val_path, 'sequences', val_vid)] = ann_path

class PathTrackData(BaseDataset):
    def __init__(self, data_dir, phase='all'):
        super(PathTrackData, self).__init__(data_dir)
        self.phase = phase 
        self.train_path = os.path.join(data_dir, 'pathtrack_release/train')
        self.test_path = os.path.join(data_dir, 'pathtrack_release/test')
        if not os.path.exists(self.train_path) or not os.path.exists(self.test_path):
            raise IOError('dataset dir: {} or {} is non-exist'.format(
                            self.train_path, self.test_path))
        
        self.load_dataset_info()

    def load_dataset_info(self):
        for train_vid in os.listdir(self.train_path):
            vid = os.path.join(self.train_path, train_vid, 'img1')
            ann = os.path.join(self.train_path, train_vid, 'gt/gt.txt')
            if self.phase == 'train' or self.phase == 'all':
                self.vid_dict[vid] = ann
        
        for val_vid in os.listdir(self.val_path):
            vid = os.path.join(self.val_path, val_vid, 'img1')
            ann = os.path.join(self.val_path, val_vid, 'gt/gt.txt')
            if self.phase == 'test' or self.phase == 'all':
                self.vid_dict[vid] = ann
        
    
class MOTData(BaseDataset):
    def __init__(self, data_dir):
        super(MOTData, self).__init__(data_dir)
        self.train_path = os.path.join(data_dir, 'train')
        
        if not os.path.exists(self.train_path):
            raise IOError('dataset dir: {} is non-exist'.format(
                            self.train_path))
        self.load_dataset_info()

    def load_dataset_info(self):
        for vid in os.listdir(self.train_path):
            self.vid_dict[os.path.join(self.train_path, vid, 'img1')] = os.path.join(self.train_path, vid, 'gt/gt.txt')
            self.n_vids += 1

class DanceTrackData(BaseDataset):
    def __init__(self, data_dir, phase='all'):
        super(DanceTrackData, self).__init__(data_dir)
        self.train_path = os.path.join(data_dir, 'train')
        self.val_path = os.path.join(data_dir, 'val')
        self.phase = phase
        if not os.path.exists(self.train_path) or not os.path.exists(self.val_path):
            raise IOError('dataset dir: {} or {} is non-exist'.format(
                            self.train_path, self.val_path))
        self.load_dataset_info()

    def load_dataset_info(self):
        for train_vid in os.listdir(self.train_path):
            vid = os.path.join(self.train_path, train_vid, 'img1')
            ann = os.path.join(self.train_path, train_vid, 'gt/gt.txt')
            if self.phase == 'train' or self.phase == 'all':
                self.vid_dict[vid] = ann
        
        for val_vid in os.listdir(self.val_path):
            vid = os.path.join(self.val_path, val_vid, 'img1')
            ann = os.path.join(self.val_path, val_vid, 'gt/gt.txt')
            if self.phase == 'val' or self.phase == 'all':
                self.vid_dict[vid] = ann

class CRDData(BaseDataset):
    def __init__(self, data_dir):
        super(CRDData, self).__init__(data_dir)
        self.vids_path = os.path.join(self.data_dir, 'comit_short_videos')
        self.anns_path = os.path.join(self.data_dir, 'comit_short_videos_labeled')
        if not os.path.exists(self.anns_path) or not os.path.exists(self.vids_path):
            raise IOError('dataset dir: {} or {} is non-exist'.format(
                            self.vids_path, self.anns_path))
        self.load_dataset_info()

    def load_dataset_info(self):
        for ann in os.listdir(self.anns_path):
            ann_path = os.path.join(self.anns_path, ann)
            vid_path = os.path.join(self.vids_path, ann)
            if os.path.isdir(ann_path) and os.path.exists(vid_path):
                self.n_vids += 1
                self.vid_dict[vid_path] = os.path.join(ann_path, 'annotations.xml')
    
    def cvat_to_mot_format(self, box_scale=1, label='face'):
        from bs4 import BeautifulSoup

        mot_data = {}
        for vid in self.vid_dict.keys():
            ann_path = self.vid_dict[vid]
            frame_dict = {}

            with open(ann_path, 'r') as f:
                data = f.read()
            bs_data = BeautifulSoup(data, 'xml')
            trackes = bs_data.find_all('track')
            width = int(bs_data.find_all('width')[0].text)
            height = int(bs_data.find_all('height')[0].text)

            for track in trackes:
                track_id = int(track['id']) + 1
                track_label = track['label']
                # group_id = track['group_id']
                if track_label != label:
                    continue

                for box in track.find_all('box'):
                    frame_id = int(box['frame']) + 1
                    occluded = box['occluded']
                    x1 = float(box['xtl'])
                    y1 = float(box['ytl'])
                    x2 = float(box['xbr'])
                    y2 = float(box['ybr'])
                    w = x2 - x1 
                    h = y2 - y1

                    new_w = w * box_scale
                    new_h = h * box_scale
                    new_x1 = x1 - ((new_w - w) / 2)
                    new_y1 = y1 - ((new_h - h) / 2)
                    x1, y1, w, h = new_x1, new_y1, new_w, new_h
                    x1 = max(0, x1)
                    y1 = max(0, y1)
                    if (x1 + w) > width: w = width - x1 
                    if (y1 + h) > height: h = height - y1 
                    
                    if frame_id not in frame_dict.keys():
                        frame_dict[frame_id] = []
                    
                    str_box = '{},{},{},{},{},{},1,-1,-1,-1\n'.format(frame_id, 
                                                                        track_id,
                                                                        x1, y1,
                                                                        w, h)
                    frame_dict[frame_id].append(str_box)
            mot_data[vid] = frame_dict
            f.close()

        return mot_data

    def export_mot_format(self, save_dir, box_scale, label):
        os.makedirs(save_dir, exist_ok=True)
        mot_data = self.cvat_to_mot_format(box_scale, label)
        for key in mot_data.keys():
            save_path = os.path.join(save_dir, key.split('/')[-1] + '.txt')
            f = open(save_path, 'w')
            
            for frame_id in mot_data[key]:
                for track in mot_data[key][frame_id]:
                    f.write(track)
            f.close()
            
            self.vid_dict[key] = save_path

if __name__ == "__main__":
    a = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking'
    b = CRDData(a)
    box_scale = 2
    label = 'face'
    save_mot_format = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking/mot_format_comit_scale_roi_{}'.format(box_scale)
    b.export_mot_format(save_mot_format, box_scale, label)
   
    # a = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/tracking_datasets/MOT16'
    # b = MOTData(a)
    # for key in b.vid_dict.keys():
    #     print(os.listdir(key))
    #     exit()