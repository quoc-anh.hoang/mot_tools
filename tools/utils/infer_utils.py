import os 
import cv2 
from tools.utils.config_utils import get_tools_elements
import shutil

def infer_one_image(image, detector, tracker):
    outputs = {'detect': None, 'tracking': None}
    if detector is not None:
        out_detect = detector([image])
        outputs['detect'] = out_detect
        if tracker is not None:
            out_track = tracker.update(out_detect[0][0], out_detect[1][0], out_detect[2][0])
            outputs['tracking'] = out_track

    return outputs

def get_gt_with_interval(data, args):
    print('Getting gt with interval {} of data {}...'.format(args.skip_frame, args.data_name))
    sequence_path_list = data.vid_dict.keys()
    skip_frame = int(args.skip_frame)
    for vid in sequence_path_list:
        if args.data_name in ['mot15', 'mot16', 'mot17', 'mot20']:
            vid_name = vid.split('/')[-2]
            save_gt_interval = os.path.join(args.save_tracking_folder, args.data_name + '_gt_interval_' + str(args.skip_frame))
            os.makedirs(save_gt_interval, exist_ok=True)
            if skip_frame == 0:
                shutil.copy(data.vid_dict[vid], save_gt_interval)
            else:
                f = open(data.vid_dict[vid], 'r')
                lines = f.readlines()
                f.close()
                save_new_ann = os.path.join(save_gt_interval, vid_name + '.txt')
                f = open(save_new_ann, 'w')
                for line in lines:
                    line = line.strip()
                    frame = int(line.split(',')[0])
                    if frame % (skip_frame + 1) == 0:
                        f.write(line + '\n')
                f.close()

        elif args.data_name in ['crd']:
            vid_name = vid.split('/')[-1]
            save_gt_interval = os.path.join(args.save_tracking_folder, args.data_name + '_gt_interval_' + str(args.skip_frame))
            os.makedirs(save_gt_interval, exist_ok=True)
            if skip_frame == 0:
                shutil.copy(data.vid_dict[vid], save_gt_interval)
            else:
                f = open(data.vid_dict[vid], 'r')
                lines = f.readlines()
                f.close()
                save_new_ann = os.path.join(save_gt_interval, vid_name + '.txt')
                f = open(save_new_ann, 'w')
                for line in lines:
                    line = line.strip()
                    frame = int(line.split(',')[0])
                    if frame % (skip_frame + 1) == 0:
                        f.write(line + '\n')
                f.close()

def get_track_predicted_mot(detector, data, args):
    print('Predicting data {}'.format(args.data_name))
    save_mot = os.path.join(args.save_tracking_folder, args.data_name + '_predict_interval_{}'.format(args.skip_frame))
    os.makedirs(save_mot, exist_ok=True)
    sequence_path_list = data.vid_dict.keys()
    for vid in sequence_path_list:
        tracker = get_tools_elements('tracking', args.tracking_type, args.tracking_config)
        vid_name = vid.split('/')[-2]
        img_list = os.listdir(vid)
        frame_id = 0
        f = open(os.path.join(save_mot, vid_name + '.txt'), 'w')

        for img_name in img_list:
            img = cv2.imread(os.path.join(vid, img_name))
            if img is None:
                continue
            frame_id += 1
            if args.skip_frame != 0:
                if frame_id % (args.skip_frame + 1) != 0:
                        continue
            
            outputs = infer_one_image(img, detector, tracker)
            if outputs['tracking'] is not None:
                if len(outputs['tracking'])  > 0:
                    track_boxes = outputs['tracking'][:, :4]
                    track_ids = outputs['tracking'][:, 4]
                    track_confs = outputs['tracking'][:, 5]
                    track_cls = outputs['tracking'][:, 6]
                    for i in range(len(track_boxes)):
                        txt_save = '{},{},{},{},{},{},1,{},1.0\n'.format(
                        frame_id, int(track_ids[i]), 
                        track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1], int(track_cls[i] + 1)
                        )
                        f.write(txt_save)
        f.close()
        tracker.clear()
        del tracker
        
def get_track_predicted_crd(detector, data, args):
    print('Predicting data {}'.format(args.data_name))
    tracking_type = args.tracking_type
    tracking_config = args.tracking_config

    crd_folder = os.path.join(args.save_tracking_folder, args.data_name + '_predict_interval_{}'.format(args.skip_frame))
    os.makedirs(crd_folder, exist_ok=True)
    vid_ext = ['.mp4', '.avi']
    # vid_folder = os.path.join(data_dir, 'comit_short_videos')
    vid_list = data.vid_dict.keys()
    for vid_name in vid_list:
        vid_name = vid_name.split('/')[-1]
        for ext in vid_ext:
            if not vid_name.endswith(ext):
                continue
            print(vid_name)
            tracker = get_tools_elements('tracking', tracking_type, tracking_config)

            f = open(os.path.join(crd_folder, vid_name + '.txt'), 'w')
            
            vid_path = os.path.join(data.vids_path, vid_name)
            vid = cv2.VideoCapture(vid_path)
            frame_id = 0
            while(True):
                ret, frame = vid.read()
                if not ret:
                    break
                frame_id += 1
                if args.skip_frame != 0:
                    if frame_id % (args.skip_frame + 1) != 0:
                            continue

                outputs = infer_one_image(frame, detector, tracker)
                if outputs['tracking'] is not None:
                    if len(outputs['tracking'])  > 0:
                        track_boxes = outputs['tracking'][:, :4]
                        track_ids = outputs['tracking'][:, 4]
                        track_confs = outputs['tracking'][:, 5]
                        track_cls = outputs['tracking'][:, 6]
                        for i in range(len(track_boxes)):
                            txt_save = '{},{},{},{},{},{},1,{},1.0\n'.format(
                            frame_id, int(track_ids[i]), 
                            track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1], int(track_cls[i] + 1)
                            )
                            f.write(txt_save)
            f.close()
            vid.release()
            tracker.clear()
            del tracker
