import os 
import cv2
import argparse
from tools.utils.config_utils import get_tools_elements
from tools.utils.infer_utils import infer_one_image
from tools.utils.visual import visual_one_image_detect, visual_one_image_tracking

def make_parser():
    parser = argparse.ArgumentParser("MOT Tools")
    
    #Test data
    parser.add_argument("--data-path", type=str, default=None)

    #Detect
    parser.add_argument("--detect-model", type=str, default=None, help="detect model name")
    parser.add_argument("--detect-config", type=str, default=None, help="path to detect config file")
    parser.add_argument("--skip-frame", type=int, default=0, help='number of frame to skip')

    #Tracking
    parser.add_argument("--tracking-type", type=str, default=None, help="name of tracking algorithm")
    parser.add_argument("--tracking-config", type=str, default=None, help="path to tracking config")

    #Visual
    parser.add_argument("--visual", action='store_true', help='visual output')

    #Save
    parser.add_argument("--save-tracking-path", type=str, default=None, help='save tracking path with MOT16 format')
    return parser

def run(args, detector, tracker):
    if os.path.isfile(args.data_path):
        img_ext = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
        vid_ext = ['.mp4', '.avi']
        for ext in img_ext:
            if args.data_path.endswith(ext):
                image = cv2.imread(args.data_path)
                outputs = infer_one_image(image, detector, tracker)
                if args.visual:
                    draw = image.copy()
                    if outputs['detect'] is not None:
                        if len(outputs['detect']) > 0:
                            draw = visual_one_image_detect(outputs['detect'][0][0], '', '', draw)
                        cv2.imshow('', draw)
                        cv2.waitKey()

        if args.save_tracking_path is not None:
            if os.path.exists(args.save_tracking_path):
                os.remove(args.save_tracking_path)
            
            f = open(args.save_tracking_path, 'w')

        for ext in vid_ext:
            if args.data_path.endswith(ext):
                vid = cv2.VideoCapture(args.data_path)
                frame_id = 0
                while(True):
                    ret, frame = vid.read()
                    if not ret:
                        break 

                    frame_id += 1
                    if args.skip_frame != 0 and frame_id % args.skip_frame != 0:
                        continue

                    outputs = infer_one_image(frame, detector, tracker)
                    draw = frame.copy()
                    if outputs['tracking'] is not None:
                        if len(outputs['tracking'])  > 0:
                            track_boxes = outputs['tracking'][:, :4]
                            track_ids = outputs['tracking'][:, 4]
                            track_confs = outputs['tracking'][:, 5]
                            track_cls = outputs['tracking'][:, 6]
                            if args.save_tracking_path:
                                for i in range(len(track_boxes)):
                                    txt_save = '{},{},{},{},{},{},1,{},1.0\n'.format(
                                    frame_id, int(track_ids[i]), 
                                    track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1], int(track_cls[i] + 1)
                                    )
                                    f.write(txt_save)
                            if args.visual:
                                draw = visual_one_image_tracking(track_boxes, track_ids, track_confs, track_cls, draw)

                    elif outputs['tracking'] is None and outputs['detect'] is not None:
                        if len(outputs['detect']) > 0 and args.visual:
                            draw = visual_one_image_detect(outputs['detect'][0][0], '', '', draw)

                    if args.visual:
                        draw = cv2.resize(draw, (draw.shape[1]//2, draw.shape[0]//2))
                        cv2.imshow('', draw)
                        if cv2.waitKey() & 0xFF == ord('q'): 
                            break

                vid.release()
                if args.visual:
                    cv2.destroyAllWindows() 

            if args.save_tracking_path:
                f.close()

def main():
    args = make_parser().parse_args()
    
    detect_model = args.detect_model
    detect_config = args.detect_config

    tracking_type = args.tracking_type
    tracking_config = args.tracking_config

    detector = get_tools_elements('detect', detect_model, detect_config)
    tracker = get_tools_elements('tracking', tracking_type, tracking_config)
    run(args, detector, tracker)

if __name__ == "__main__":
    main()