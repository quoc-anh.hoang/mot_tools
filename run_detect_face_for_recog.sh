python detect_face.py \
--detect-model yolov5-face \
--detect-config configs/yolov5-face.yaml \
--tracking-type  '' \
--tracking-config configs/bytetrack.yaml \
--skip-frame 4 \
# --save-tracking-folder output_tracking_txt/bytetrack/detect_0.6_high_0.4_match_0.7_buffer_10