import sys 
sys.path.append(".")

import torch
import cv2
import time 
import numpy as np 

from tools.person_detect.yolov5.model import YOLOv5Person
from tools.face_detect.yolov5.model import YOLOv5Face
from tools.utils.visual import visual_one_image_detect, visual_one_image_tracking

from tools.tracking.bytetrack.byte_tracker import BYTETracker
from tools.tracking.sparsetrack.sparse_tracker import SparseTracker


if __name__ == "__main__":
    
    detect_thresh = 0.7
    track_high_thresh = 0.4
    detect_frame_interval = 4
    match_thresh = 0.6
    track_buffer = 25

    person_model_path = 'models/person_detect/yolov8/v8s_ver3_13-6-2023_with_coco_dancetrack.onnx'
    face_model_path = 'models/face_detect/yolov5-face/v5n_vsviet_batch1.onnx'
    test_image_path = "test_images/1.png"
    test_image_path_2 = "test_images/3.png"
    test_video_path = '/home/hqanh/Documents/ALL_Person_dataset/person/VideoTest/tu_quay/test_tracking_case/di_thang/vlc-record-2023-11-20-10h22m19s-vlc-record-2023-11-07-16h22m35s-rtsp___192.168.0.82_554_H.264_media.smp-.mp4-.mp4'

    person_model = YOLOv5Person(person_model_path)
    face_model = YOLOv5Face(face_model_path, conf_thresh=0.05, scale_roi=2)
    byte_tracker = BYTETracker(track_thresh=detect_thresh, high_thresh=track_high_thresh, match_thresh=match_thresh, track_buffer=track_buffer)
    sparse_tracker = SparseTracker(detect_thresh=detect_thresh, high_thresh=track_high_thresh, match_thresh=match_thresh, track_buffer=track_buffer)
    vid = cv2.VideoCapture(test_video_path) 
    frame_id = 0
    out_byte_path = 'output_tracking_txt/byte_vlc-record-2023-11-20-10h22m19s-vlc-record-2023-11-07-16h22m35s-rtsp___192.168.0.82_554_H.264_media.smp-.txt'
    out_sparse_path = 'output_tracking_txt/sparse_vlc-record-2023-11-20-10h22m19s-vlc-record-2023-11-07-16h22m35s-rtsp___192.168.0.82_554_H.264_media.smp-.txt'
    
    byte_file = open(out_byte_path, 'w')
    sparse_file = open(out_sparse_path, 'w')
    # result = cv2.VideoWriter('out_video_test/byte_yolov5.avi',  
    #                      cv2.VideoWriter_fourcc(*'XVID'), 
    #                      29, (540, 960)) 
    
    fps = vid.get(cv2.CAP_PROP_FPS)
    print("FPS: ", fps)
    while(True): 
        
        # Capture the video frame 
        # by frame 
        ret, frame = vid.read() 
        if not ret:
            break
        
        out_bytetrack = []
        out_sparsetrack = []

        frame_id += 1
        if frame_id % (detect_frame_interval + 1) != 0:
            continue

        if frame_id % (detect_frame_interval + 1) == 0:
            out_face = face_model([frame])
            out_person = person_model([frame])
        
            out_bytetrack = byte_tracker.update(out_face[0][0], out_face[1][0], out_face[2][0])
            out_sparsetrack = sparse_tracker.update(out_face[0][0], out_face[1][0], out_face[2][0])

        if len(out_bytetrack) > 0:
            track_boxes = out_bytetrack[:, :4]
            track_ids = out_bytetrack[:, 4]
            track_confs = out_bytetrack[:, 5]
            track_cls = out_bytetrack[:, 6]
            # for i in range(len(track_boxes)):
            #     txt_byte = '{},{},{},{},{},{},1,-1,-1,-1\n'.format(
            #         frame_id, track_ids[i], 
            #         track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1]
            #         )
            #     byte_file.write(txt_byte)
            draw_byte = visual_one_image_tracking(track_boxes, track_ids, track_confs, track_cls, frame)
        else:
            draw_byte = frame
        
        if len(out_sparsetrack) > 0:
            track_boxes = out_sparsetrack[:, :4]
            track_ids = out_sparsetrack[:, 4]
            track_confs = out_sparsetrack[:, 5]
            track_cls = out_sparsetrack[:, 6]
            # for i in range(len(track_boxes)):
            #     txt_sparse = '{},{},{},{},{},{},1,-1,-1,-1\n'.format(
            #             frame_id, track_ids[i], 
            #             track_boxes[i][0], track_boxes[i][1], track_boxes[i][2] - track_boxes[i][0], track_boxes[i][3] - track_boxes[i][1]
            #             )
                # sparse_file.write(txt_sparse)
            draw_sparse = visual_one_image_tracking(track_boxes, track_ids, track_confs, track_cls, frame)
        else:
            draw_sparse = frame
        # draw = visual_one_image_detect(out_person[0][0], '', '', draw)
        draw_sparse = cv2.resize(draw_sparse, (int(draw_sparse.shape[1] / 2), int(draw_sparse.shape[0] / 2)), interpolation = cv2.INTER_LINEAR)
        draw_byte = cv2.resize(draw_byte, (int(draw_byte.shape[1] / 2), int(draw_byte.shape[0] / 2)), interpolation = cv2.INTER_LINEAR)
        # vis = np.concatenate((draw_byte, draw_sparse), axis=0)
        # result.write(draw_byte) 
        cv2.imshow('byte', draw_byte) 
        cv2.imshow('sparse', draw_sparse) 
        if cv2.waitKey(0) & 0xFF == ord('q'): 
            break
    
    # After the loop release the cap object 
    vid.release() 
    # result.release() 

    # Destroy all the windows 
    cv2.destroyAllWindows() 

    
    # image = cv2.imread(test_image_path)
    # draw = image.copy()

    # image2 = cv2.imread(test_image_path_2)
    # draw2 = image2.copy()

    # t = time.time()
    # out = face_model([image, image])
    # draw = visual_one_image_detect(out[0][0], '', '', image)
    # cv2.imshow('', draw)
    # cv2.waitKey()