import os 
import shutil 

def main():
    parent_eval_folder = 'TrackEval/dataset_test/crd_face/bytetrack_custom1'
    gt_track = 'output_tracking_txt/bytetrack_custom1/detect_0.6_high_0.4_match_0.8_buffer_10/crd_gt_interval_3'
    predict_track = 'output_tracking_txt/bytetrack_custom1/detect_0.6_high_0.4_match_0.8_buffer_10/crd_predict_interval_3'
    sub_folder = gt_track.split('/')[-2] + '/' + gt_track.split('/')[-1]

    seqmaps = os.path.join(parent_eval_folder, sub_folder, 'GT_FOLDER/seqmaps')
    os.makedirs(seqmaps, exist_ok=True)
    seqmaps_f = open(os.path.join(seqmaps, 'MOT16-train.txt'), 'w')
    seqmaps_f.write('name\n')

    vid_name_list = os.listdir(gt_track)
    for vid_name in vid_name_list:
        gt_vid_eval = os.path.join(parent_eval_folder, sub_folder, 'GT_FOLDER/MOT16-train', vid_name[:-4], 'gt')
        os.makedirs(gt_vid_eval, exist_ok=True)
        seqinfo = os.path.join(parent_eval_folder, sub_folder, 'GT_FOLDER/MOT16-train', vid_name[:-4], 'seqinfo.ini')
        shutil.copy(os.path.join(gt_track, vid_name), os.path.join(gt_vid_eval, 'gt.txt'))

        f = open(seqinfo, 'w')
        f.write('[Sequence]\n')
        f.write('name=' + vid_name[:-4] + '\n')
        f.write('imDir=img1\n')
        f.write('frameRate=30\n')
        f.write('seqLength=1050\n')
        f.write('imWidth=1920\n')
        f.write('imHeight=1080\n')
        f.write('imExt=.jpg\n')
        f.close()

        seqmaps_f.write(vid_name[:-4] + '\n')
    seqmaps_f.close()

    data_path = os.path.join(parent_eval_folder, sub_folder, 'TRACKER_FOLDER/MOT16-train/crd/data')
    os.makedirs(data_path, exist_ok=True)
    for vid_name in os.listdir(predict_track):
        shutil.copy(os.path.join(predict_track, vid_name), data_path)

if __name__ == '__main__':
    main()