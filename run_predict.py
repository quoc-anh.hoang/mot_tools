import os 
import cv2 
import argparse
from tools.utils.config_utils import get_tools_elements
from tools.utils.infer_utils import infer_one_image, get_gt_with_interval

def make_parser():
    parser = argparse.ArgumentParser("MOT Tools")
    
    #Test data
    parser.add_argument("--data-name", type=str, default=None)
    parser.add_argument("--data-path", type=str, default=None)
    
    #Detect
    parser.add_argument("--detect-model", type=str, default=None, help="detect model name")
    parser.add_argument("--detect-config", type=str, default=None, help="path to detect config file")
    parser.add_argument("--skip-frame", type=int, default=0, help='number of frame to skip')

    #Tracking
    parser.add_argument("--tracking-type", type=str, default=None, help="name of tracking algorithm")
    parser.add_argument("--tracking-config", type=str, default=None, help="path to tracking config")

    #Save
    parser.add_argument("--save-tracking-folder", type=str, default=None, help='save tracking path with MOT16 format')
    
    return parser

def get_track_predicted(args, detector):
    if args.data_name == 'crd':
        from datasets.datasets import CRDData
        from tools.utils.infer_utils import get_track_predicted_crd
        data = CRDData(args.data_path)
        print('Loaded CRD data')
        save_mot_format = '/home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking/mot_format_comit_scale_roi_{}'.format(detector.scale_roi)
        data.export_mot_format(save_mot_format, detector.scale_roi, label='face')
        print('Exported CRD to MOT format')
        get_gt_with_interval(data, args)
        get_track_predicted_crd(detector, data, args)

    elif args.data_name in ['mot15', 'mot16', 'mot17', 'mot20']:
        from datasets.datasets import MOTData
        from tools.utils.infer_utils import get_track_predicted_mot
        data = MOTData(args.data_path)
        print('Loaded MOT data')
        get_gt_with_interval(data, args)
        get_track_predicted_mot(detector, data, args)

    else:
        print('Invalid data name: {}'.format(args.data_name))

def main():
    args = make_parser().parse_args()
    detect_model = args.detect_model
    detect_config = args.detect_config
    tracking_type = args.tracking_type
    tracking_config = args.tracking_config

    detector = get_tools_elements('detect', detect_model, detect_config)
    # tracker = get_tools_elements('tracking', tracking_type, tracking_config)

    get_track_predicted(args, detector)

if __name__ == '__main__':
    main() 