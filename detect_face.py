import os 
import cv2
import numpy as np
from tools.utils.config_utils import get_tools_elements
from tools.utils.infer_utils import infer_one_image
import argparse

def make_parser():
    parser = argparse.ArgumentParser("MOT Tools")
    
    #Test data
    # parser.add_argument("--data-path", type=str, default=None)

    #Detect
    parser.add_argument("--detect-model", type=str, default=None, help="detect model name")
    parser.add_argument("--detect-config", type=str, default=None, help="path to detect config file")
    parser.add_argument("--skip-frame", type=int, default=0, help='number of frame to skip')

    #Tracking
    parser.add_argument("--tracking-type", type=str, default=None, help="name of tracking algorithm")
    parser.add_argument("--tracking-config", type=str, default=None, help="path to tracking config")

    #Visual
    parser.add_argument("--visual", action='store_true', help='visual output')

    #Save
    parser.add_argument("--save-tracking-path", type=str, default=None, help='save tracking path with MOT16 format')
    return parser

def face_alignment(image, landmarks):
    src = np.array([
            [38.2946, 51.6963], 
            [73.5318, 51.5014], 
            [56.0252, 71.7366],
            [41.5493, 92.3655], 
            [70.7299, 92.2041]], dtype=np.float32)
    M, _ = cv2.estimateAffinePartial2D(landmarks.reshape(1,5,2), src.reshape(1,5,2))
    warped = cv2.warpAffine(image, M, (112, 112), borderValue=0.0)
    # warped = cv2.cvtColor(warped, cv2.COLOR_BGR2RGB)
    return warped

def run(args, detector, tracker):
    data_folder = '/home/hqanh/Videos/test_db'
    id_list = os.listdir(data_folder)
    save_folder = data_folder + '_alignment'
    os.makedirs(save_folder, exist_ok=True)
    for id in id_list:
        print(id)
        os.makedirs(os.path.join(save_folder, id), exist_ok=True)
        image_list = os.listdir(os.path.join(data_folder, id))
        for img_name in image_list:
            img_path = os.path.join(data_folder, id, img_name)
            img = cv2.imread(img_path)
            if img is None:
                continue
            outputs = infer_one_image(img, detector, tracker)
            
            for i, box in enumerate(outputs['detect'][0][0]):
                x1 = int(box[0])
                y1 = int(box[1])
                x2 = int(box[2])
                y2 = int(box[3])
                new_img = img[y1:y2, x1:x2]
                if new_img is None:
                    continue
                if min(new_img.shape) == 0:
                    continue

                # img = cv2.rectangle(img, (x1, y1), (x2, y2), (0,0,255), 10) 

                landmarks = outputs['detect'][3][0][i]
                # right_mouth = (int(landmarks[6]), int(landmarks[7]))
                # left_mouth = (int(landmarks[8]), int(landmarks[9]))
                # right_eye = (int(landmarks[0]), int(landmarks[1]))
                # left_eye = (int(landmarks[2]), int(landmarks[3]))
                # nose = (int(landmarks[4]), int(landmarks[5]))

                # new_img = cv2.circle(new_img, right_mouth, radius=5, color=(0, 0, 255), thickness=-1)
                # new_img = cv2.circle(new_img, left_mouth, radius=5, color=(0, 0, 255), thickness=-1)
                # new_img = cv2.circle(new_img, right_eye, radius=5, color=(0, 0, 255), thickness=-1)
                # new_img = cv2.circle(new_img, left_eye, radius=5, color=(0, 0, 255), thickness=-1)
                # new_img = cv2.circle(new_img, nose, radius=5, color=(0, 0, 255), thickness=-1)

                align_img = face_alignment(new_img, landmarks)
                cv2.imwrite(os.path.join(save_folder, id, str(i) + '_' + img_name), align_img)
    
def main():
    args = make_parser().parse_args()
    
    detect_model = args.detect_model
    detect_config = args.detect_config

    tracking_type = args.tracking_type
    tracking_config = args.tracking_config

    detector = get_tools_elements('detect', detect_model, detect_config)
    tracker = None
    run(args, detector, tracker)

if __name__ == '__main__':
    main()