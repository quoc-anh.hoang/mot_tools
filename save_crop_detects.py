import os 
from tools.utils.config_utils import get_tools_elements
from tools.utils.infer_utils import infer_one_image
import cv2 
import argparse
import numpy as np 

def face_alignment(image, landmarks):
    src = np.array([
            [38.2946, 51.6963], 
            [73.5318, 51.5014], 
            [56.0252, 71.7366],
            [41.5493, 92.3655], 
            [70.7299, 92.2041]], dtype=np.float32)
    M, _ = cv2.estimateAffinePartial2D(landmarks.reshape(1,5,2), src.reshape(1,5,2))
    warped = cv2.warpAffine(image, M, (112, 112), borderValue=0.0)
    # warped = cv2.cvtColor(warped, cv2.COLOR_BGR2RGB)
    return warped

def make_parser():
    parser = argparse.ArgumentParser("MOT Tools")
    
    #Test data
    parser.add_argument("--data-path", type=str, default=None)

    #Detect
    parser.add_argument("--detect-model", type=str, default=None, help="detect model name")
    parser.add_argument("--detect-config", type=str, default=None, help="path to detect config file")
    parser.add_argument("--skip-frame", type=int, default=0, help='number of frame to skip')

    #Tracking
    parser.add_argument("--tracking-type", type=str, default=None, help="name of tracking algorithm")
    parser.add_argument("--tracking-config", type=str, default=None, help="path to tracking config")

    #Visual
    parser.add_argument("--visual", action='store_true', help='visual output')

    #Save
    parser.add_argument("--save-tracking-path", type=str, default=None, help='save tracking path with MOT16 format')
    return parser

def save_one_video(model, video_path, save_crop_folder):
    print(video_path)
    video_name = video_path.split('/')[-1][:-4]
    save_path = os.path.join(save_crop_folder, video_name)
    if not os.path.exists(save_path):
        os.makedirs(save_path, exist_ok=True)
    else:
        print('haved save_path')
        return 0
    
    vid = cv2.VideoCapture(video_path)
    frame_id = 0
    while(True):
        ret, frame = vid.read()
        if not ret:
            break 
        outputs = infer_one_image(frame, model, None)
        frame_id += 1
        if outputs['detect'] is None:
            continue

        boxes = outputs['detect'][0][0]
        for i, box in enumerate(boxes):
            image_name = 'frame_{}_id_{}.jpg'.format(frame_id, i)
            image_path = os.path.join(save_path, image_name)
            w = box[2] - box[0]
            h = box[3] - box[1]
            # if h < 100 or w < 40:
            #     continue
            landmarks = outputs['detect'][3][0][i]
            crop = frame[int(box[1]):int(box[3]), int(box[0]):int(box[2])]
            align_img = face_alignment(crop, landmarks)

            cv2.imwrite(image_path, align_img)
        
    vid.release()

def main():
    args = make_parser().parse_args()
    save_path = 'out_video_test'
    detector = get_tools_elements('detect', args.detect_model, args.detect_config)
    save_one_video(detector, args.data_path, save_path)

if __name__ == "__main__":
    main()