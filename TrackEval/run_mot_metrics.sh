python  scripts/run_mot_challenge.py \
--GT_FOLDER /home/hqanh/HQA/mot_tools/TrackEval/data_samples/gt/mot_challenge \
--TRACKERS_FOLDER /home/hqanh/HQA/mot_tools/TrackEval/data_samples/trackers/mot_challenge \
--TRACKER_SUB_FOLDER data \
--BENCHMARK MOT16 \
--SPLIT_TO_EVAL train \
--DO_PREPROC False