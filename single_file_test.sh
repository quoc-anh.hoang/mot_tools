python single_file_test.py \
--data-path /home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking/comit_short_videos/vlc-record-2023-11-24-09h45m06s-vlc-record-2023-11-22-14h17m49s-rtsp___192.168.6.112_554_H.264_media.smp-.mp4-.mp4 \
--detect-model yolov5-face \
--detect-config configs/yolov5-face.yaml \
--tracking-type  bytetrack \
--tracking-config configs/bytetrack.yaml \
--visual \
--skip-frame 0 