python run_predict.py \
--data-name crd \
--data-path /home/hqanh/Documents/ALL_Person_dataset/object_tracking/test_dataset/face_tracking \
--detect-model yolov5-face \
--detect-config configs/yolov5-face.yaml \
--tracking-type  bytetrack \
--tracking-config configs/bytetrack.yaml \
--skip-frame 0 \
--save-tracking-folder output_tracking_txt