# MOT_Tools

# Tổng quan:

Tạo một bộ tool hỗ trợ test các thuật toán detection và tracking gốc và tự custom trên video thực tế hoặc các bộ dữ liệu có sẵn. Đối với các bộ dữ liệu có sẵn thì sẽ trả về giá trị các metric dựa trên mô hình detect và tracking.

# Test với 1 video

sh single_file_test.sh

# Dự đoán các bộ dữ liệu

sh run_predict.sh

Đối với bộ dữ liệu face crd thì sẽ convert từ cvat format sang mot format trước. Đầu ra dự đoán và chuyển từ annotation gốc sang dạng có frame interval đều nằm trong save-tracking-folder

# Note

Đầu vào của các mô hình detect là list các ảnh (kể cả có 1 ảnh cũng cho vào list) vd: [image1, image2, ...]

Đầu ra của các mô hình detect có dạng: [[box1, box2, ...], [box1, box2,...]], [[conf1, conf2,...], [conf1, conf2,...]], [[class1, class2,...], [class1, class2,...]]

Đầu vào của các thuật toán tracking có dạng: [box1, box2,...], [conf1, conf2, ...], [class1, class2,...]

Đầu ra của các thuật toán tracking có dạng: [[x1, y1, x2, y2, id, score, class], [x1, y1, x2, y2, id, score, class], ...]

Tất cả trong list có dạng numpy array

Các thuật toán tracking cần 1 phương thức clear() để reset lại các thông tin của các đối tượng liên quan (vd: trong bytetrack cần reset lại Basetrack, tránh trường hợp sang video khác mà id tiếp tục tăng)

# Tính metrics

cd vào thư mục TrackEval và đọc readme

Repo gốc để tính các metrics tại [đây](https://github.com/JonathonLuiten/TrackEval)

